export const CREATED = 'CREATED';
export const PAID = 'PAID';
export const AUTHORIZED = 'AUTHORIZED';
export const CANCELED = 'CANCELED';
export const SHIPPING = 'SHIPPING';
export const COMPLETED = 'COMPLETED';
export const EXPIRED = 'EXPIRED';
export const PENDING = 'PENDING';
export const REFUNDED = 'REFUNDED';
export const ERROR = 'ERROR';

export const ALL_SUCCESS = [CREATED, PAID, AUTHORIZED, SHIPPING, COMPLETED, PENDING]


export const getStatusMessage = (status) => {
    switch (status) {
        case CREATED:
            return __('Your order has been created.');
        case PAID:
            return __('Your order has been paid.');
        case AUTHORIZED:
            return __('Your order has been authorized.');
        case CANCELED:
            return __('Your order has been canceled.');
        case SHIPPING:
            return __('Your order is shipping.');
        case COMPLETED:
            return __('Your order is completed.');
        case EXPIRED:
            return __('Your order has expired.');
        case PENDING:
            return __('Your order is pending.');
        case REFUNDED:
            return __('Your payment has been refunded.');
        case ERROR:
            return __('There was an error processing your order.')
    }
};
