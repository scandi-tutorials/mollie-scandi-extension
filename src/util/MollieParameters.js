export const getParameters = () => {
    const url = new URL(window.location.href);
    const params = url.searchParams;

    return {
        orderId: url.searchParams.get("order_id"),
        paymentToken: url.searchParams.get("mollie_payment_token"),
        orderHash: url.searchParams.get("mollie_order_hash"),
    };
}
