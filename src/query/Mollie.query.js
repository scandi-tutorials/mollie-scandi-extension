import { Field } from 'Util/Query';

/** @namespace Query/Mollie */
export class MollieQuery {
    getProcessTransactionMutation(paymentToken) {
        if (!paymentToken) {
            throw Error("The payment_token is required")
        }

        return new Field('mollieProcessTransaction')
            .addArgument('input', 'MollieProcessTransactionInput', { payment_token: paymentToken })
            .addFieldList(['paymentStatus', this._getCartField()])
    }

    _getCartField() {
        return new Field('cart').addField('id')
    }

    getMollieRestoreCartMutation(cartId) {
        if (!cartId) {
            throw Error("The cart_id is required to restore the cart")
        }

        return new Field('mollieRestoreCart')
            .addArgument('input', 'MollieResetCartInput', { cart_id: cartId })

    }
}

export default new MollieQuery()
