import { ALL_SUCCESS, getStatusMessage } from '../util/PaymentStatus';
import CheckoutSuccess from 'Component/CheckoutSuccess';
import Loader from 'Component/Loader';


export const MOLLIE_PROCESSING_STEP = 'MOLLIE_PROCESSING_STEP';

function renderMollieStep() {
    const { mollie: { isLoading, paymentStatus } = {}, orderID } = this.props;

    if (isLoading) {
        return <Loader isLoading/>
    }

    if (ALL_SUCCESS.includes(paymentStatus)) {
        return (
            <CheckoutSuccess
                orderID={ orderID }
            />
        );
    }

    return false;
}

const stepMap = (member, instance) => ({
    ...member,
    [MOLLIE_PROCESSING_STEP]: {
        title: __('Loading'),
        url: '/mollie_result',
        render: renderMollieStep.bind(instance),
        areTotalsVisible: false,
    },
});

const renderTitle = (args, callback, instance) => {
    const { checkoutStep, mollie: { isLoading, paymentStatus } = {} } = instance.props;

    if (checkoutStep !== MOLLIE_PROCESSING_STEP) {
        return callback(...args);
    }

    if (isLoading) {
        return (
            <h2 block="Checkout" elem="Title">
                { __("Loading, please wait") }
            </h2>
        );
    }

    const message = getStatusMessage(paymentStatus);

    return (
        <h2 block="Checkout" elem="Title">
            { message }
        </h2>
    );
};

export default {
    'Route/Checkout/Component': {
        'member-property': {
            stepMap,
        },
        'member-function': {
            renderTitle,
        },
    },
}
