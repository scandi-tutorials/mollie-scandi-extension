const _getOrderField = (args, callback, instance) => {
    return callback()
        .addFieldList([
            'mollie_redirect_url',
            'mollie_payment_token'
        ])
}


export default {
    "Query/Checkout": {
        "member-function": {
            _getOrderField
        }
    }
}
