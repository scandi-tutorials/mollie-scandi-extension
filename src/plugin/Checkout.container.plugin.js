import CheckoutQuery from 'Query/Checkout.query';
import MollieQuery from '../query/Mollie.query';
import { isSignedIn } from 'Util/Auth';
import { getGuestQuoteId } from 'Util/Cart';
import { fetchMutation } from 'Util/Request';
import {
    DETAILS_STEP, PAYMENT_TOTALS,
} from 'Route/Checkout/Checkout.config';
import BrowserDatabase from 'Util/BrowserDatabase';


import { MOLLIE_METHODS } from '../util/MollieMethods';
import { redirectToUrl } from '../util/Redirect'
import { getParameters } from '../util/MollieParameters';
import { MOLLIE_PROCESSING_STEP } from './Checkout.component.plugin';
import { getPaymentToken, setPaymentToken } from '../util/PaymentTokenPersistence';

const __construct = (args, callback, instance) => {
    const [props] = args;
    const {
        toggleBreadcrumbs,
    } = props;

    const { orderId, paymentToken, orderHash } = getParameters();

    if (!paymentToken) {
        return callback(...args)
    }

    toggleBreadcrumbs(false);

    instance.state = {
        isLoading: false,
        isDeliveryOptionsLoading: false,
        requestsSent: 0,
        paymentMethods: [],
        shippingMethods: [],
        shippingAddress: {},
        checkoutStep: MOLLIE_PROCESSING_STEP,
        orderID: orderId,
        paymentTotals: BrowserDatabase.getItem(PAYMENT_TOTALS) || {},
        email: '',
        isGuestEmailSaved: false,
        isCreateUser: false,
        estimateAddress: {},
        mollieParameters: { orderId, paymentToken, orderHash },
        mollie: { isLoading: true },
    };
};

const componentDidMount = (args, callback, instance) => {
    const { mollieParameters: { paymentToken: isMolliePayment } = {} } = instance.state;

    if (!isMolliePayment) {
        return callback(...args)
    }

    const paymentToken = getPaymentToken();
    const processTransactionMutation = MollieQuery.getProcessTransactionMutation(paymentToken);
    fetchMutation(processTransactionMutation).then(({mollieProcessTransaction: { paymentStatus, cart }}) => {
        instance.setState({
            mollie: {
                isLoading: false,
                paymentStatus,
                cart,
            },
        })
    })
};

const savePaymentMethodAndPlaceOrder = async (args, callback, instance) => {
    const [paymentInformation] = args;

    const { paymentMethod: { code, additional_data } } = paymentInformation;
    const guest_cart_id = !isSignedIn() ? getGuestQuoteId() : '';


    if (!MOLLIE_METHODS.includes(code)) {
        return await callback(...args);
    }

    try {
        await fetchMutation(CheckoutQuery.getSetPaymentMethodOnCartMutation({
            guest_cart_id,
            payment_method: {
                code,
                [code]: additional_data,
            },
        }));

        const orderData = await fetchMutation(CheckoutQuery.getPlaceOrderMutation(guest_cart_id));
        const { placeOrder: { order: { mollie_redirect_url, mollie_payment_token } } } = orderData;

        if(Boolean(mollie_payment_token)) {
            // Workaround:
            // If I understand correctly (though this doesn't seem to be documented) the payment_token specified as
            // a URL parameter in the return URL should be the same as the one we have here. However, it seems that
            // it is in fact different, and incorrect. To work around this bug, we store the correct payment token
            // here, so we can use it later.
            // (using Mollie_Magento2 version 1.19.0, with a custom URL format)
            setPaymentToken(mollie_payment_token);
        } else {
            throw Error("Expected mollie_payment_token in order data, none found", orderData)
        }

        if (Boolean(mollie_redirect_url)) {
            redirectToUrl(mollie_redirect_url)
        } else {
            throw Error("Expected mollie_redirect_url in order data, none found", orderData)
        }

        // redirects or throws above
    } catch (e) {
        instance._handleError(e);
    }
};


export default {
    "Route/Checkout/Container": {
        "member-function": {
            __construct,
            componentDidMount,
            savePaymentMethodAndPlaceOrder,
        },
    },
}
